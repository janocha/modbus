package pl.polsl.model;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import pl.polsl.view.gui.controllers.MenuController;
import pl.polsl.view.gui.controllers.StoreMenuController;

public class SlaveModbusExecutor implements ModbusExecutor {
    
    private byte address;

    public SlaveModbusExecutor(byte address) {
        this.address = address;
    }

    public byte getAddress() {
        return address;
    }

    public void setAddress(byte address) {
        this.address = address;
    }
    
    public ModbusFrame execute(ModbusFrame frame) {
        if (isValid(frame)) {
            if (frame.getFunction() == SEND) {
                return processSendRequest(frame);
            } else if (frame.getFunction() == GET) {
                return processGetRequest(frame);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public boolean isValid(ModbusFrame frame) {
        if (frame.getAddress() == ModbusFrameBuilder.BROADCAST
                || frame.getAddress() == address) {
            return true;
        } else {
            return false;
        }
    }

	////////////asaaaaaaaaaaaaaaaaaaaaaaaa
    private ModbusFrame processGetRequest(ModbusFrame frame) {
        if (frame.getAddress() == ModbusFrameBuilder.BROADCAST) {
            return null;
        }
        try {
        ModbusFrame response = new ModbusFrame();
        response.setAddress(address);
        response.setFunction(frame.getFunction());
        
        
        MenuController menuController=StoreMenuController.menuController;
		
        response.setData(menuController.getTexttextAreaWyslij().getBytes());
        menuController.setTexttextFieldRamkaWyslana(ModbusFrameBuilder.stringToHex(ModbusFrameBuilder.serialize(response)));
        menuController.setTexttextFieldRamkaOdebrana(ModbusFrameBuilder.stringToHex(ModbusFrameBuilder.serialize(frame)));
        return response;
	}catch(Exception e){
	System.out.println("exeption slave processgetrequest");
}
//        response.setData(IWSKView.tfMessage.getText().getBytes());
//
//        IWSKView.tfGetFrame.setText(ModbusFrameBuilder.stringToHex(ModbusFrameBuilder.serialize(frame)));
//        IWSKView.tfSendFrame.setText(ModbusFrameBuilder.stringToHex(ModbusFrameBuilder.serialize(response)));
        
        //zmienic null na response
        return null;
    }

    private ModbusFrame processSendRequest(ModbusFrame frame) {
        if (frame.getAddress() == address ||
                frame.getAddress() == ModbusFrameBuilder.BROADCAST) {
            ModbusFrame response = new ModbusFrame();
            response.setAddress(address);
            response.setFunction(frame.getFunction());
            response.setData(new byte[]{});
            System.out.println("test");
            try {
            	MenuController menuController=StoreMenuController.menuController;

        		String tmp1=new String(frame.getData(),"UTF-8");
        		String tmp2=new String();
        		tmp2= (menuController.getTexttextAreaWyswietlana() + tmp1 + "\r\n");
        		menuController.setTexttextAreaWyswietlana(tmp2);
        		String tmp3=new String();
        		tmp3= (ModbusFrameBuilder.stringToHex(ModbusFrameBuilder.serialize(frame)));
        		menuController.setTexttextFieldRamkaOdebrana(tmp3);
        		
        		String tmp4=new String();
        		tmp4= (ModbusFrameBuilder.stringToHex(ModbusFrameBuilder.serialize(response)));
            if (frame.getAddress() != ModbusFrameBuilder.BROADCAST) {
            	menuController.setTexttextFieldRamkaWyslana(tmp4);
            }else {
            	menuController.setTexttextFieldRamkaWyslana("nic");
          }
		}catch(Exception e){
		System.out.println("exeption slave processsendrequest");
	}
//            IWSKView.tfMessageWindow.setText(IWSKView.tfMessageWindow.getText()
//                    + new String(frame.getData()) + "\r\n");
//            IWSKView.tfGetFrame.setText(ModbusFrameBuilder.stringToHex(ModbusFrameBuilder.serialize(frame)));
//            if (frame.getAddress() != ModbusFrameBuilder.BROADCAST) {
//                IWSKView.tfSendFrame.setText(ModbusFrameBuilder.stringToHex(ModbusFrameBuilder.serialize(response)));
//            } else {
//                IWSKView.tfSendFrame.setText("");
//            }
            return response;
        } else {
            return null;
        }
    }
}
