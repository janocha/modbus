package pl.polsl.model;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import pl.polsl.view.gui.controllers.MenuController;
import pl.polsl.view.gui.controllers.StoreMenuController;

public class MasterModbusExecutor implements ModbusExecutor {

	private ModbusFrame request;

	public ModbusFrame execute(ModbusFrame frame) {
		if (isValid(frame)) {
			if (frame.getFunction() == SEND) {
				return processSendRequest(frame);
			} else if (frame.getFunction() == GET) {
				return processGetRequest(frame);
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public boolean isValid(ModbusFrame frame) {
		if ((frame.getAddress() == request.getAddress()) && frame.getFunction() == request.getFunction()) {
			return true;
		} else {
			return false;
		}
	}

	private ModbusFrame processGetRequest(ModbusFrame frame) {
		try {
			MenuController menuController=StoreMenuController.menuController;
			menuController.setTexttextAreaWyswietlana(menuController.getTexttextAreaWyswietlana() + new String(frame.getData()) + "\r\n");
			menuController.setTexttextFieldRamkaWyslana(ModbusFrameBuilder.stringToHex(ModbusFrameBuilder.serialize(request)));
			menuController.setTexttextFieldRamkaOdebrana(ModbusFrameBuilder.stringToHex(ModbusFrameBuilder.serialize(frame)));
//		MenuController.textAreaWyswietlana.setText(MenuController.textAreaWyswietlana.getText() + new String(frame.getData()) + "\r\n");
//		MenuController.textFieldRamkaWyslana.setText(ModbusFrameBuilder.stringToHex(ModbusFrameBuilder.serialize(request)));
//		MenuController.textFieldRamkaOdebrana.setText(ModbusFrameBuilder.stringToHex(ModbusFrameBuilder.serialize(frame)));
        
//		IWSKView.tfMessageWindow.setText(IWSKView.tfMessageWindow.getText() + new String(frame.getData()) + "\r\n");
//		IWSKView.tfGetFrame.setText(ModbusFrameBuilder.stringToHex(ModbusFrameBuilder.serialize(frame)));
//		IWSKView.tfSendFrame.setText(ModbusFrameBuilder.stringToHex(ModbusFrameBuilder.serialize(request)));
        System.out.println("test");
		}catch(Exception e){
			System.out.println("exeption master processgetrequest");
		}
		return new ModbusFrame();
	}
	@FXML
	private Label labelKodRozkazu;
	private ModbusFrame processSendRequest(ModbusFrame frame) {
		labelKodRozkazu.setVisible(false);
		try {
			MenuController menuController=StoreMenuController.menuController;
			menuController.setTexttextAreaWyswietlana(menuController.getTexttextAreaWyswietlana() + "Potwierdzono operacje." + "\r\n");
			menuController.setTexttextFieldRamkaWyslana(ModbusFrameBuilder.stringToHex(ModbusFrameBuilder.serialize(request)));
			menuController.setTexttextFieldRamkaOdebrana(ModbusFrameBuilder.stringToHex(ModbusFrameBuilder.serialize(frame)));
			
//			MenuController.textAreaWyswietlana.setText(MenuController.textAreaWyswietlana.getText() + "Potwierdzono operacje." + "\r\n");
//			MenuController.textFieldRamkaWyslana.setText(ModbusFrameBuilder.stringToHex(ModbusFrameBuilder.serialize(request)));
//			MenuController.textFieldRamkaOdebrana.setText(ModbusFrameBuilder.stringToHex(ModbusFrameBuilder.serialize(frame)));
//		IWSKView.tfMessageWindow.setText(IWSKView.tfMessageWindow.getText() + "Potwierdzono operacje." + "\r\n");
//		IWSKView.tfGetFrame.setText(ModbusFrameBuilder.stringToHex(ModbusFrameBuilder.serialize(frame)));
//		IWSKView.tfSendFrame.setText(ModbusFrameBuilder.stringToHex(ModbusFrameBuilder.serialize(request)));
        System.out.println("test");
		}catch(Exception e){
		System.out.println("exeption master processsendreguest");
	}
		return new ModbusFrame();

	}

	public boolean hasResponse(ModbusFrame frame) {
		if (frame.getAddress() == ModbusFrameBuilder.BROADCAST) {
			return false;
		} else {
			return true;
		}
	}

	public ModbusFrame getRequest() {
		return request;
	}

	public void setRequest(ModbusFrame request) {
		this.request = request;
	}
}
